# 基于Java Swing的职工信息管理系统

## 介绍
在职工信息管理系统中，主要是对职工信息管理，功能分下如下：

1.职工信息的添加

2.职工信息的查询(按工号进行查询)

3.职工信息的删除(按工号进行删除)

4.职工信息的修改(按工号进行修改)

## 相关技术
1.  Java的Swing编程
2.  Java的JDBC编程


## 所需环境
1.  JDK1.7
2.  Access 2016


## 安装教程
遇到问题可以到**相关博客**进行查看

1.  **运行之前需要配置好ODBC**
2.  检查JDK版本，必须是JDK1.7：
```
java -version
```

3.  进入`src`目录，编译
```
javac -encoding UTF-8 StaffInfoManage.java
```

4.  运行
```
java StaffInfoManage
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/094154_32e29682_1590078.png "屏幕截图.png")

注意：如果修改源代码后需要删除所有.class文件，在编译运行！
```
del *.class
```

## 运行截图

1.主页面

![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/094218_b62c2beb_1590078.png "1.主页面.png")

2.查询

![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/094225_55494849_1590078.png "2.查询.png")

3.菜单栏

![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/094231_f09426ff_1590078.png "3.菜单栏.png")

4.添加

![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/094237_f273d117_1590078.png "4.添加.png")

5.删除

![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/094244_ddf0840c_1590078.png "5.删除.png")

6.修改

![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/094250_c1686a41_1590078.png "6.修改.png")

7.关于

![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/094258_44be84cd_1590078.png "7.关于.png")


## 相关博客
1.  [Microsoft Access 2016安装教程](https://blog.csdn.net/WeiHao0240/article/details/120672363)
2.  [Java使用ODBC连接Access数据库](https://blog.csdn.net/WeiHao0240/article/details/120727203)
3.  [TextPad安装环境配置](https://jackwei.blog.csdn.net/article/details/86914950)
4.  [IDEA运行Java Swing项目中文乱码](https://blog.csdn.net/WeiHao0240/article/details/120744954)
5.  [Java指令编译java文件](https://blog.csdn.net/WeiHao0240/article/details/120778832)
6.  [Java运行程序找不到ODBC驱动](https://blog.csdn.net/WeiHao0240/article/details/120879107)